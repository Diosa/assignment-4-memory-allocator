#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#if defined(Diosa_WINDOWS)

#  include <windows.h>

#else

#  include <unistd.h>

#endif

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#if defined(Diosa_WINDOWS)

static size_t getpagesize(void) {
    SYSTEM_INFO si;
    GetSystemInfo(&si);
    return si.dwAllocationGranularity;
}

#endif

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

//Возвращает, хватает ли памяти в блоке
static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

//Возвращает количество свободных регионов памяти
static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

//Возвращает суммарное количество памяти в свободных регионах
static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

//Возвращает, какое количество памяти будет выделено
static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);



static void* map_pages(void const* addr, size_t length, int additional_flags) {
#if defined(Diosa_WINDOWS)
    return VirtualAlloc((void *) addr, length, MEM_COMMIT, PAGE_READWRITE);
#else
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
#endif
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
    /* ??? */
    size_t actual_query = region_actual_size(size_from_capacity((block_capacity) { query }).bytes);
    struct region* new_region = map_pages(addr, actual_query, MAP_FIXED_NOREPLACE);
    if (new_region == MAP_FAILED || new_region == NULL) {
        new_region = map_pages(addr, actual_query, 0);
        if (new_region == MAP_FAILED || new_region == NULL) {
            return REGION_INVALID;
        }
    }
    struct region result_region = (struct region) {.addr = new_region, .size = actual_query, .extends = true};
        if (new_region != addr) {
            result_region.extends = false;
        }
    block_init(result_region.addr, (block_size) {result_region.size}, NULL);
    return result_region;
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

// query - размер требуемого блока (capacity - без заголовка)
static bool split_if_too_big(struct block_header *block, size_t query) {
    /* ??? */
    if (block == NULL) {
        return false;
    }

    if (!block_splittable(block, query)){
        return false;
    }

    block_init(block->contents + query,
               (block_size) {block->capacity.bytes - (block_capacity) {query}.bytes},
               block->next);

    block->capacity = (block_capacity) {query};
    block->next = (struct block_header*) (block->contents + query);
    return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
    if (block != NULL) {
        return (void *) (block->contents + block->capacity.bytes);
    } else {
        return NULL;
    }
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
    /*  ??? */
    if (block == NULL) {
        return false;
    }

    if (block->next == NULL || !mergeable(block, block->next)) {
        return false;
    }
    block->capacity.bytes += offsetof(struct block_header, contents) + block->next->capacity.bytes;
    block->next = block->next->next;
    return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};


static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    /* ??? */
    while (true) {
        if (!block->is_free) {
            while (!block->is_free) {
                if (block->next != NULL) {
                    block = block->next;
                } else {
                    return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = block};
                }
            }
        }
        if (!block_is_big_enough(sz, block)) {
            while (try_merge_with_next(block));
        }
        if (block_is_big_enough(sz, block)) {
            return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = block};
        }
        if (block->next != NULL) {
            block = block->next;
        } else {
            break;
        }
    }
    return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = block};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    /* ??? */
    if (block == NULL) {
        return (struct block_search_result) {.block = block, .type = BSR_CORRUPTED};
    }

    struct block_search_result bsr = find_good_or_last(block, query);
    if (bsr.type == BSR_REACHED_END_NOT_FOUND || bsr.type == BSR_CORRUPTED) {
        return bsr;
    } else {
        while (try_merge_with_next(bsr.block));
        split_if_too_big(bsr.block, query);
        bsr.block->is_free = false;
        return bsr;
    }
}

static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    /*  ??? */
    if (last == NULL) {
        return NULL;
    }

    struct region new_region = alloc_region(block_after(last), query);
    if (region_is_invalid(&new_region)) {
        return NULL;
    }

    last->next = (struct block_header*) new_region.addr;
    if (!try_merge_with_next(last)) {
        return new_region.addr;
    }

    return last;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    /*  ??? */
    query = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result search_result = try_memalloc_existing(query, heap_start);
    if (search_result.type != BSR_FOUND_GOOD_BLOCK) {
        struct block_header* last = grow_heap(search_result.block, query);
        if (last == NULL) {
            return NULL;
        } else {
            search_result = try_memalloc_existing(query, last);
            if (search_result.type != BSR_FOUND_GOOD_BLOCK) {
                return NULL;
            }
        }
    }

    if (search_result.type == BSR_FOUND_GOOD_BLOCK) {
        return search_result.block;
    } else {
        return NULL;
    }
}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    /*  ??? */
    while (try_merge_with_next(header));
}
