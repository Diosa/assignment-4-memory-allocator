#define HEAP_SIZE 5000
#define HUGE 50000
#define _DEFAULT_SOURCE

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//Обычное успешное выделение памяти.
int simple_malloc_test(void) {
    printf("\n--- 1: simple_malloc ---\n");

    size_t init_size = 1200;
    void *head = heap_init(HEAP_SIZE);

    void *memory = _malloc(init_size);

    if (memory != NULL) {
        printf("success\n");
        _free(memory);
        munmap(head, size_from_capacity((block_capacity){.bytes = HEAP_SIZE}).bytes);
        return 0;
    } else {
        printf("fail\n");
        return 1;
    }
}

//Освобождение одного блока из нескольких выделенных.
int free_one_block_test() {
    printf("\n--- 2: one_block_free ---\n");
    size_t init_size = 1200;
    void *head = heap_init(HEAP_SIZE);

    void *mem_part1 = _malloc(init_size);
    void *mem_part2 = _malloc(init_size);

    if (mem_part1 == NULL || mem_part2 == NULL) {
        printf("fail\n");
        return 1;
    }
    _free(mem_part2);
    if (mem_part2 == NULL && mem_part1 != NULL) {
        _free(mem_part1);
        munmap(head, size_from_capacity((block_capacity){.bytes = HEAP_SIZE}).bytes);
        printf("success\n");
        return 0;
    } else {
        munmap(head, size_from_capacity((block_capacity){.bytes = HEAP_SIZE}).bytes);
        printf("fail\n");
        return 1;
    }
}

//Освобождение двух блоков из нескольких выделенных.
int free_two_block_from_several_test() {
    printf("\n--- 3: two_block_from_several_free ---\n");
    size_t init_size = 1200;
    void *head = heap_init(HEAP_SIZE);

    void *mem_part1 = _malloc(init_size);
    void *mem_part2 = _malloc(init_size);
    void *mem_part3 = _malloc(init_size);

    _free(mem_part2);
    _free(mem_part3);
    if (mem_part3 == NULL && mem_part2 == NULL && mem_part1 != NULL) {
        _free(mem_part1);
        munmap(head, size_from_capacity((block_capacity){.bytes = HEAP_SIZE}).bytes);
        printf("success\n");
        return 0;
    } else {
        munmap(head, size_from_capacity((block_capacity){.bytes = HEAP_SIZE}).bytes);
        printf("fail\n");
        return 1;
    }
}

//Память закончилась, новый регион памяти расширяет старый.
void grow_heap_without_space_test() {
    printf("\n--- 4: new region extends old region ---\n");

    size_t init_size = 1200;
    void* head = heap_init(HEAP_SIZE);

    void* over_heap_block = _malloc(HEAP_SIZE * 2);
    printf("Heap overflow\n");

    _free(over_heap_block);
    munmap(head, size_from_capacity((block_capacity){.bytes = HEAP_SIZE}).bytes);
    printf("Heap free\n");
}

//Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.
void grow_heap_with_space_test() {
    printf("\n--- 5: new region not extends old region ---\n");

    void* head = heap_init(HEAP_SIZE);
    struct block_header* first = (struct block_header*) head;


    void* over_heap_block = _malloc(first->capacity.bytes);

    struct block_header* second = (struct block_header*) (over_heap_block - offsetof(struct block_header, contents));

    void *after_heap = (void *) second->contents + second->capacity.bytes;

    void* temp_map = mmap(after_heap,
                          HEAP_SIZE,
                          PROT_READ | PROT_WRITE,
                          MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS,
                          -1,
                          0);

    void* last = _malloc(1000);
    printf("success\n");
    _free(over_heap_block);
    _free(last);
    munmap(temp_map, size_from_capacity((block_capacity){.bytes = HEAP_SIZE}).bytes);
    munmap(head, size_from_capacity((block_capacity){.bytes = HEAP_SIZE}).bytes);
}
