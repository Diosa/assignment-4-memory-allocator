#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <stdio.h>

#define HEAP_SIZE 5000
#define HUGE 50000

int simple_malloc_test(void);
int free_one_block_test();
int free_two_block_from_several_test();
void grow_heap_without_space_test();
void grow_heap_with_space_test();
