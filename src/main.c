#include "tests.h"

int main(void) {
    simple_malloc_test();
    free_one_block_test();
    free_two_block_from_several_test();
    grow_heap_without_space_test();
    grow_heap_with_space_test();
}
